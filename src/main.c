#include <zephyr.h>

void delay() {
  k_sleep(100);
}
void main() {
  volatile int i = 0;
  while (1) {
    i += 1;
    k_sleep(100);
  }
}
