# Configuration

# Initial configuration
```
mkdir -p $HOME/.virtualenv
python3 -mvenv $HOME/.virtualenv/zephyr
. $HOME/.virtualenvs/zephyr/bin/activate
pip install west
pip install -r zephyr/scripts/requirements.txt
```

# Configuring your build tree
```
west update
```

# Building
```
west build
west flash
```
You may need to add local configuration; for example, to set the
default runner. If so, copy `local.cmake.example` to `local.cmake` and
configure to taste.

# License
This board spec is licensed under Apache v2

Everything else is GPLv2 or later.
